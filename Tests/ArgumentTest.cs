﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2021, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using FluentAssertions;
using Xunit;


// ReSharper disable PossibleNullReferenceException

namespace XploRe.Diagnostics.Contracts.Tests
{

    public class ArgumentTest
    {

        [Fact]
        public void EnsureNotNull_ThrowOnNull_Struct()
            => Assert.Throws<ArgumentNullException>(() => Argument.EnsureNotNull((int?) null));

        [Fact]
        public void EnsureNotNull_ThrowOnNull_WithNullParamName()
            // ReSharper disable once RedundantArgumentDefaultValue
            => Assert.Throws<ArgumentNullException>(() => Argument.EnsureNotNull((object) null, null))
                     .ParamName.Should().BeNull();

        [Fact]
        public void EnsureNotNull_ThrowOnNull_WithParamName()
        {
            string parameter = null;

            Assert.Throws<ArgumentNullException>(() => Argument.EnsureNotNull(parameter, nameof(parameter)))
                  .ParamName.Should().Be(nameof(parameter));
        }

        [Fact]
        public void EnsureNotNull_ThrowOnNull_WithParamNameViaCallerArgumentExpression()
        {
            string parameter = null;

            Assert.Throws<ArgumentNullException>(() => Argument.EnsureNotNull(parameter))
                  .ParamName.Should().Be(nameof(parameter));
        }

    }

}
