/*
 * xplo.re .NET
 *
 * Copyright (C) 2021, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;


namespace XploRe.Diagnostics.Contracts
{

    /// <summary>
    ///     Argument validation helpers.
    /// </summary>
    public static class Argument
    {

        /// <summary>
        ///     Throws an <see cref="ArgumentNullException" /> if <paramref name="argument" /> is <see langword="null" />.
        /// </summary>
        /// <param name="argument">Argument to validate as non-<see langref="null" />.</param>
        /// <param name="paramName">The name of the parameter with which <paramref name="argument" /> corresponds.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void EnsureNotNull<T>([NotNull] T? argument, [CallerArgumentExpression("argument")] string? paramName = null)
        {
            if (argument is null) {
                ThrowArgumentNullException(paramName);
            }
        }

        [DoesNotReturn]
        private static void ThrowArgumentNullException(string? paramName) =>
            throw new ArgumentNullException(paramName);

    }

}
