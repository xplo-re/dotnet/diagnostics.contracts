/*
 * xplo.re .NET
 *
 * Copyright (C) 2021, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

// ReSharper disable CheckNamespace
// ReSharper disable EmptyNamespace

namespace System.Runtime.CompilerServices
{

    /*
     * Similar to nullable handling, the compiler only validates the attribute name but not the full type name (i.e.
     * including the assembly) for caller attributes; defining these internally works for components targeting older
     * frameworks as well.
     *
     * References:
     * https://docs.microsoft.com/en-us/dotnet/api/system.runtime.compilerservices.callerargumentexpressionattribute?view=net-5.0
     */

#if !NET5_0_OR_GREATER && !NETCOREAPP3_0_OR_GREATER

    /// <summary>
    ///     Allows to automatically capture the expressions of an argument that is passed to a method.
    /// </summary>
    /// <remarks>
    ///     Natively supported on:
    ///     <list type="bullet">
    ///         <item>.NET 5.0 or higher</item>
    ///         <item>.NET Core 3.0 and 3.1</item>
    ///     </list>
    ///     Language support:
    ///     <list type="bullet">
    ///         <item>C# 10 or higher</item>
    ///         <item>Visual Basic 17 or higher</item>
    ///     </list>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = false)]
    internal sealed class CallerArgumentExpressionAttribute : Attribute
    {

        /// <summary>
        ///     Initialises a new <see cref="CallerArgumentExpressionAttribute" /> instance.
        /// </summary>
        /// <param name="parameterName">The name of the targeted parameter to capture the argument expression of.</param>
        public CallerArgumentExpressionAttribute(string parameterName)
        {
            ParameterName = parameterName;
        }

        /// <summary>
        ///     Gets the name of the targeted parameter.
        /// </summary>
        /// <returns>The name of the targeted parameter.</returns>
        public string ParameterName { get; }

    }

#endif

}
