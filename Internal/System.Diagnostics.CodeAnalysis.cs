/*
 * xplo.re .NET
 *
 * Copyright (C) 2021, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

// ReSharper disable CheckNamespace
// ReSharper disable EmptyNamespace

namespace System.Diagnostics.CodeAnalysis
{

    /*
     * C# 8.0 and later automatically generate nullable attribute classes for the type system (NullableAttribute and
     * NullableContextAttribute) which are directly embedded into the assembly. This is not the case for additional
     * attributes though that do affect nullable handling but must be specified explicitly. As in either case the
     * compiler only validates the attribute name but not the full type name (i.e. including the assembly), defining
     * additional attribute classes internally works for older frameworks as well.
     *
     * References:
     * https://docs.microsoft.com/en-us/dotnet/api/system.diagnostics.codeanalysis?view=net-5.0
     * https://github.com/dotnet/runtime/tree/main/src/libraries/System.Private.CoreLib/src/System/Diagnostics/CodeAnalysis
     */

#if !NET5_0_OR_GREATER && !NETCOREAPP3_0_OR_GREATER && !NETSTANDARD2_1_OR_GREATER
    /// <summary>
    ///     Specifies that <see langword="null" /> is allowed as an input even if the corresponding type disallows it.
    /// </summary>
    /// <remarks>
    ///     Natively supported on:
    ///     <list type="bullet">
    ///         <item>.NET 5.0 or higher</item>
    ///         <item>.NET Core 3.0 and 3.1</item>
    ///         <item>.NET Standard 2.1</item>
    ///     </list>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.Property, Inherited = false)]
    internal sealed class AllowNullAttribute : Attribute
    { }

    /// <summary>
    ///     Specifies that <see langword="null" /> is disallowed as an input even if the corresponding type allows it.
    /// </summary>
    /// <remarks>
    ///     Natively supported on:
    ///     <list type="bullet">
    ///         <item>.NET 5.0 or higher</item>
    ///         <item>.NET Core 3.0 and 3.1</item>
    ///         <item>.NET Standard 2.1</item>
    ///     </list>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.Property, Inherited = false)]
    internal sealed class DisallowNullAttribute : Attribute
    { }

    /// <summary>
    ///     Specifies a method that will never return under any circumstance.
    /// </summary>
    /// <remarks>
    ///     Natively supported on:
    ///     <list type="bullet">
    ///         <item>.NET 5.0 or higher</item>
    ///         <item>.NET Core 3.0 and 3.1</item>
    ///         <item>.NET Standard 2.1</item>
    ///     </list>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    internal sealed class DoesNotReturnAttribute : Attribute
    { }

    /// <summary>
    ///     Specifies a method that will not return if the associated <see cref="Boolean" /> parameter is passed the
    ///     specified value.
    /// </summary>
    /// <remarks>
    ///     Natively supported on:
    ///     <list type="bullet">
    ///         <item>.NET 5.0 or higher</item>
    ///         <item>.NET Core 3.0 and 3.1</item>
    ///         <item>.NET Standard 2.1</item>
    ///     </list>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Parameter, Inherited = false)]
    internal sealed class DoesNotReturnIfAttribute : Attribute
    {

        /// <summary>
        ///     Initialises the attribute with the specified parameter value.
        /// </summary>
        /// <param name="parameterValue">
        ///     The condition parameter value. Code after the associated method will be considered unreachable if the
        ///     argument to the associated parameter matches this value.
        /// </param>
        public DoesNotReturnIfAttribute(bool parameterValue)
        {
            ParameterValue = parameterValue;
        }

        /// <summary>
        ///     Gets the condition parameter value.
        /// </summary>
        public bool ParameterValue { get; }

    }

    /// <summary>
    ///     Specifies that an output may be <see langword="null" /> even if the corresponding type disallows it.
    /// </summary>
    /// <remarks>
    ///     Natively supported on:
    ///     <list type="bullet">
    ///         <item>.NET 5.0 or higher</item>
    ///         <item>.NET Core 3.0 and 3.1</item>
    ///         <item>.NET Standard 2.1</item>
    ///     </list>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.Property | AttributeTargets.ReturnValue, Inherited = false)]
    internal sealed class MaybeNullAttribute : Attribute
    { }

    /// <summary>
    ///     Specifies that when the associated method returns <see cref="ReturnValue" />, the associated parameter may
    ///     be <see langword="null" /> even if the corresponding type disallows it.
    /// </summary>
    /// <remarks>
    ///     Natively supported on:
    ///     <list type="bullet">
    ///         <item>.NET 5.0 or higher</item>
    ///         <item>.NET Core 3.0 and 3.1</item>
    ///         <item>.NET Standard 2.1</item>
    ///     </list>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Parameter, Inherited = false)]
    internal sealed class MaybeNullWhenAttribute : Attribute
    {

        /// <summary>
        ///     Initialises the attribute with the specified return value condition.
        /// </summary>
        /// <param name="returnValue">
        ///     The return value condition. If the associated method returns this value, the associated parameter may be
        ///     <see langword="null" />.
        /// </param>
        public MaybeNullWhenAttribute(bool returnValue)
        {
            ReturnValue = returnValue;
        }

        /// <summary>
        ///     Gets the return value condition.
        /// </summary>
        public bool ReturnValue { get; }

    }

    /// <summary>
    ///     If applied to a return value: specifies that an output is not <see langword="null" /> even if the
    ///     corresponding type allows it.
    ///     <p>
    ///         If applied to a parameter: specifies that an input argument was not <see langword="null" /> when the
    ///         call returns.
    ///     </p>
    /// </summary>
    /// <remarks>
    ///     Natively supported on:
    ///     <list type="bullet">
    ///         <item>.NET 5.0 or higher</item>
    ///         <item>.NET Core 3.0 and 3.1</item>
    ///         <item>.NET Standard 2.1</item>
    ///     </list>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.Property | AttributeTargets.ReturnValue, Inherited = false)]
    internal sealed class NotNullAttribute : Attribute
    { }

    /// <summary>
    ///     Specifies that the associated output will be non-<see langword="null" /> if the referenced parameter is not
    ///     <see langword="null" />.
    /// </summary>
    /// <remarks>
    ///     Natively supported on:
    ///     <list type="bullet">
    ///         <item>.NET 5.0 or higher</item>
    ///         <item>.NET Core 3.0 and 3.1</item>
    ///         <item>.NET Standard 2.1</item>
    ///     </list>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Parameter | AttributeTargets.Property | AttributeTargets.ReturnValue, AllowMultiple = true, Inherited = false)]
    internal sealed class NotNullIfNotNullAttribute : Attribute
    {

        /// <summary>
        ///     Initialises the attribute with the referenced parameter name.
        /// </summary>
        /// <param name="parameterName">
        ///     The name of the referenced parameter. The output will be non-<see langword="null" /> if the argument to
        ///     the referenced parameter is non-<see langword="null" />.
        /// </param>
        public NotNullIfNotNullAttribute(string parameterName)
        {
            ParameterName = parameterName;
        }

        /// <summary>
        ///     Gets the name of the referenced parameter.
        /// </summary>
        public string ParameterName { get; }

    }

    /// <summary>
    ///     Specifies that when a method returns <see cref="ReturnValue" />, the associated parameter will not be
    ///     <see langword="null" /> even if the corresponding type allows it.
    /// </summary>
    /// <remarks>
    ///     Natively supported on:
    ///     <list type="bullet">
    ///         <item>.NET 5.0 or higher</item>
    ///         <item>.NET Core 3.0 and 3.1</item>
    ///         <item>.NET Standard 2.1</item>
    ///     </list>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Parameter, Inherited = false)]
    internal sealed class NotNullWhenAttribute : Attribute
    {

        /// <summary>
        ///     Initialises the attribute with the specified return value condition.
        /// </summary>
        /// <param name="returnValue">
        ///     The return value condition. If the method returns this value, the associated parameter will not be
        ///     <see langword="null" />.
        /// </param>
        public NotNullWhenAttribute(bool returnValue)
        {
            ReturnValue = returnValue;
        }

        /// <summary>
        ///     Gets the return value condition.
        /// </summary>
        public bool ReturnValue { get; }

    }

#endif

#if !NET5_0_OR_GREATER
    /// <summary>
    ///     Flags for types of dynamically accessed members.
    /// </summary>
    /// <remarks>
    ///     Natively supported on:
    ///     <list type="bullet">
    ///         <item>.NET 5.0 or higher</item>
    ///     </list>
    /// </remarks>
    [Flags]
    internal enum DynamicallyAccessedMemberTypes
    {

        /// <summary>
        ///     No members.
        /// </summary>
        None = 0,

        /// <summary>
        ///     The default, parameterless public constructor.
        /// </summary>
        PublicParameterlessConstructor = 0x0001,

        /// <summary>
        ///     All public constructors.
        /// </summary>
        PublicConstructors = 0x0002 | PublicParameterlessConstructor,

        /// <summary>
        ///     All non-public constructors.
        /// </summary>
        NonPublicConstructors = 0x0004,

        /// <summary>
        ///     All public methods.
        /// </summary>
        PublicMethods = 0x0008,

        /// <summary>
        ///     All non-public methods.
        /// </summary>
        NonPublicMethods = 0x0010,

        /// <summary>
        ///     All public fields.
        /// </summary>
        PublicFields = 0x0020,

        /// <summary>
        ///     All non-public fields.
        /// </summary>
        NonPublicFields = 0x0040,

        /// <summary>
        ///     All public nested types.
        /// </summary>
        PublicNestedTypes = 0x0080,

        /// <summary>
        ///     All non-public nested types.
        /// </summary>
        NonPublicNestedTypes = 0x0100,

        /// <summary>
        ///     All public properties.
        /// </summary>
        PublicProperties = 0x0200,

        /// <summary>
        ///     All non-public properties.
        /// </summary>
        NonPublicProperties = 0x0400,

        /// <summary>
        ///     All public events.
        /// </summary>
        PublicEvents = 0x0800,

        /// <summary>
        ///     All non-public events.
        /// </summary>
        NonPublicEvents = 0x1000,

        /// <summary>
        ///     All interfaces implemented by the type.
        /// </summary>
        Interfaces = 0x2000,

        /// <summary>
        ///     All members.
        /// </summary>
        All = ~None,

    }

    /// <summary>
    ///     Indicates that a defined set of members on a specified <see cref="Type" /> are accessed dynamically, e.g.
    ///     through reflection.
    /// </summary>
    /// <remarks>
    ///     This attribute is valid on members whose type is <see cref="Type" /> or <see cref="string" />. In case of a
    ///     <see cref="string" />, the string value is interpreted as the fully qualified name of a type.
    ///     <para>
    ///         If applied to a class, interface, or struct, the members specified can be accessed dynamically on the
    ///         <see cref="Type" /> instances returned from calling <see cref="object.GetType" /> on instances of that
    ///         class, interface, or struct.
    ///     </para>
    ///     <para>
    ///         If applied to a method, the attribute applies to the <see langword="this" /> parameter of the method. As
    ///         such the attribute should only be used on instance methods of types assignable to <see cref="Type" />.
    ///     </para>
    /// </remarks>
    /// <remarks>
    ///     Natively supported on:
    ///     <list type="bullet">
    ///         <item>.NET 5.0 or higher</item>
    ///     </list>
    /// </remarks>
    [AttributeUsage(
        AttributeTargets.Field | AttributeTargets.GenericParameter | AttributeTargets.Parameter | AttributeTargets.Property |
        AttributeTargets.Method | AttributeTargets.ReturnValue |
        AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Struct,
        Inherited = false
        )]
    internal sealed class DynamicallyAccessedMembersAttribute : Attribute
    {

        /// <summary>
        ///     Initialises the attribute with the specified member types flags.
        /// </summary>
        /// <param name="memberTypes">The types of members dynamically accessed.</param>
        public DynamicallyAccessedMembersAttribute(DynamicallyAccessedMemberTypes memberTypes)
        {
            MemberTypes = memberTypes;
        }

        /// <summary>
        ///     Gets the <see cref="DynamicallyAccessedMemberTypes" /> flags that specify the type of members dynamically
        ///     accessed.
        /// </summary>
        public DynamicallyAccessedMemberTypes MemberTypes { get; }

    }

    /// <summary>
    ///     Specifies that the method or property will ensure that the referenced field and property members have
    ///     non-<see langword="null" /> values.
    /// </summary>
    /// <remarks>
    ///     Natively supported on:
    ///     <list type="bullet">
    ///         <item>.NET 5.0 or higher</item>
    ///     </list>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
    internal sealed class MemberNotNullAttribute : Attribute
    {

        /// <summary>
        ///     Initialises the attribute with the name of a referenced field or property member.
        /// </summary>
        /// <param name="member">
        ///     The name of a referenced field or property member that is promised to be non-<see langword="null" />.
        /// </param>
        public MemberNotNullAttribute(string member)
        {
            Members = new[] { member };
        }

        /// <summary>
        ///     Initialises the attribute with a list of names of referenced field and property members.
        /// </summary>
        /// <param name="members">
        ///     A list of names of referenced field and property members that are promised to be non-<see langword="null" />.
        /// </param>
        public MemberNotNullAttribute(params string[] members)
        {
            Members = members;
        }

        /// <summary>
        ///     Gets the list of names of referenced field and property members.
        /// </summary>
        public string[] Members { get; }

    }

    /// <summary>
    ///     Specifies that the method or property will ensure that the referenced field and property members have
    ///     non-<see langword="null" /> values when the method returns with the specified return value condition.
    /// </summary>
    /// <remarks>
    ///     Natively supported on:
    ///     <list type="bullet">
    ///         <item>.NET 5.0 or higher</item>
    ///     </list>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
    internal sealed class MemberNotNullWhenAttribute : Attribute
    {

        /// <summary>
        ///     Initialises the attribute with a specified return value condition and the name of a referenced field or
        ///     property member.
        /// </summary>
        /// <param name="returnValue">
        ///     The return value condition. If the method returns this value, the referenced field or property member
        ///     will not be <see langword="null" />.
        /// </param>
        /// <param name="member">
        ///     The name of a referenced field or property member that is promised to be non-<see langword="null" />
        ///     when the method returns with the specified <paramref name="returnValue" />.
        /// </param>
        public MemberNotNullWhenAttribute(bool returnValue, string member)
        {
            ReturnValue = returnValue;
            Members = new[] { member };
        }

        /// <summary>
        ///     Initialises the attribute with a specified return value condition and a list of names of referenced
        ///     field and property members.
        /// </summary>
        /// <param name="returnValue">
        ///     The return value condition. If the method returns this value, the referenced field or property members
        ///     will not be <see langword="null" />.
        /// </param>
        /// <param name="members">
        ///     A list of names of referenced field and property members that are promised to be non-<see langword="null" />
        ///     when the method returns with the specified <paramref name="returnValue" />.
        /// </param>
        public MemberNotNullWhenAttribute(bool returnValue, params string[] members)
        {
            ReturnValue = returnValue;
            Members = members;
        }

        /// <summary>
        ///     Gets the return value condition.
        /// </summary>
        public bool ReturnValue { get; }

        /// <summary>
        ///     Gets the list of names of field and property members.
        /// </summary>
        public string[] Members { get; }

    }
#endif

}
