stages:
  - build
  - test
  - pack
  - deploy

variables:
  # PROJECT CONFIGURATION
  #
  # Path to project sources, relative to the repository root. Can contain globs. Must not be empty.
  SourcePath: '.'
  # Path for build artefacts, relative to the source. Used by MSBuild implicitly.
  BaseOutputPath: 'bin'
  # Path for intermediate objects, relative to the source. Used by MSBuild implicitly.
  BaseIntermediateOutputPath: 'obj'
  # Build configuration name for debug builds. Used for unit tests and coverage reports.
  DebugBuildConfiguration: 'Debug'
  # Build configuration name for release builds. Used for unit tests and deployments.
  ReleaseBuildConfiguration: 'Release'

  # CI-RELATED
  #
  # Cache directory for NuGet packages, relative to project root.
  NuGetPackagesPath: '.nuget'

  # ENVIRONMENT VARIABLES
  #
  # Avoid shallow clone, as otherwise the total number of commits for versioning cannot be determined.
  GIT_DEPTH: 0
  # Path to NuGet packages cache. Used by NuGet and must be absolute.
  NUGET_PACKAGES: '$CI_PROJECT_DIR/$NuGetPackagesPath'


# Default job configuration. Caches intermediate objects such as NuGet packages per stage & branch.
default:
  image: mcr.microsoft.com/dotnet/sdk:6.0
  cache:
    # Cache per stage and branch.
    key: '$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG'
    # Use latest cache if available and update cache after job is completed.
    policy: pull-push
    paths:
      # Packages dependency tree with package versions, frameworks etc., including their restore location.
      - '$SourcePath/$BaseIntermediateOutputPath/project.assets.json'
      # NuGet and MSBuild related intermediate files.
      - '$SourcePath/$BaseIntermediateOutputPath/*.csproj.nuget.*'
      # NuGet cache that contains all dependencies.
      - '$NuGetPackagesPath'


# Build project for both debug and release configurations. Ensures that the project can be built before other jobs are
# started. Build artefacts are transferred to dependent jobs.
dotnet-build:
  stage: build
  before_script:
    - dotnet restore
  script:
    - dotnet build --configuration "$DebugBuildConfiguration" --no-restore
    - dotnet build --configuration "$ReleaseBuildConfiguration" --no-restore
  artifacts:
    paths:
      - '$SourcePath/$BaseOutputPath'
      - '$SourcePath/$BaseIntermediateOutputPath/$DebugBuildConfiguration'
      - '$SourcePath/$BaseIntermediateOutputPath/$ReleaseBuildConfiguration'
    expire_in: 1 month


# Test runner template. Runs unit tests for a given build configuration. Coverage is collected but not exposed. Expects
# the variable BuildConfiguration set with the name of the build configuration to use.
.dotnet-test:
  stage: test
  image: registry.gitlab.com/xplo-re/docker/dotnet/sdk-runtimes:6.0-6.0-5.0-3.1-2.1
  needs:
    - job: dotnet-build
      artifacts: true
  before_script:
    - dotnet tool restore
    - dotnet restore
    - dotnet restore Tests
  script:
    # Run unit tests with code coverage via dotCover.
    - dotnet dotcover test --settings=.config/coverage.runsettings --dcxml=.config/coverage.xml
                           --configuration="$BuildConfiguration" --no-restore  Tests
    # Add framework to class names to help GitLab differentiate between the JUnit reports.
    - for junit in reports/*.junit.xml; do
          xmlstarlet ed -L -u '//testcase/@classname' -x 'concat(.," ('$( echo $junit | awk -F. '{ print $(NF-2) }' )')")' "$junit";
      done
  artifacts:
    when: always
    paths:
      - ./reports/
    expire_in: 3 years
    reports:
      junit:
        - ./reports/**/*.junit.xml

# Test runner template with coverage exposure. Runs unit tests for a given build configuration and prints out a coverage
# total. Expects the variable BuildConfiguration set with the name of the build configuration to use.
.dotnet-test-coverage:
  extends: .dotnet-test
  after_script:
    # Convert detailed dotCover coverage report to Cobertura format for GitLab processing.
    - dotnet reportgenerator -reports:reports/dotCover.xml -targetdir:reports/ -reporttypes:Cobertura
    # Print coverage percentage from dotCover summary report for GitLab to extract. End line with an explicit LF!
    - xmlstarlet sel -t -o 'Coverage=' -m '//CoveragePercentage[1]/Statements/@Percent' -v . -o '%' -n reports/Summary.xml
  coverage: '/^Coverage=(\d+(?:\.\d+)?)%$/'
  artifacts:
    reports:
      cobertura:
        - ./reports/Cobertura.xml

dotnet-test:debug:
  extends: .dotnet-test-coverage
  variables:
    BuildConfiguration: $DebugBuildConfiguration

dotnet-test:release:
  extends: .dotnet-test
  variables:
    BuildConfiguration: $ReleaseBuildConfiguration


# Builds packages for deployment.
dotnet-pack:
  stage: pack
  needs:
    - job: dotnet-build
      artifacts: true
  before_script:
    - dotnet restore
  script:
    - dotnet pack --configuration "$ReleaseBuildConfiguration" --no-restore --no-build --include-symbols
  artifacts:
    paths:
      - '$SourcePath/$BaseOutputPath/$ReleaseBuildConfiguration/*.nupkg'
      - '$SourcePath/$BaseOutputPath/$ReleaseBuildConfiguration/*.snupkg'
    expire_in: 1 week


# NuGet base for package deployment. Requires a successful release test run.
.nuget-push:
  stage: deploy
  needs:
    - job: dotnet-pack
      artifacts: true
    - job: dotnet-test:release
  # Do not use cache.
  inherit:
    default: false
  # Skip Git fetch.
  variables:
    GIT_STRATEGY: none
  # Git not needed, use smaller Alpine image.
  image: mcr.microsoft.com/dotnet/sdk:6.0-alpine

# Push every package to GitLab package repository.
nuget-push:gitlab:
  extends: .nuget-push
  environment:
    name: NuGet GitLab Project
  before_script:
    - dotnet nuget add source "$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/nuget/index.json"
                              --name GitLab --username gitlab-ci-token --password "$CI_JOB_TOKEN" --store-password-in-clear-text
  script:
    - dotnet nuget push "$SourcePath/$BaseOutputPath/*/*.nupkg" --source GitLab --skip-duplicate

# Push every package to staging environment.
nuget-push:staging:
  extends: .nuget-push
  environment:
    name: NuGet Staging
  script:
    - dotnet nuget push "$SourcePath/$BaseOutputPath/*/*.nupkg"
                        --source "$NUGET_STAGING_SOURCE" --skip-duplicate --api-key "$NUGET_STAGING_API_KEY"
